using System;
using UnityEngine;

public struct MenuItem {
	public MenuActivities Activity;
	public string Text;
	
	public MenuItem(MenuActivities activity, string Text) {
		Activity 	= activity;
		this.Text 	= Text;
	}
}