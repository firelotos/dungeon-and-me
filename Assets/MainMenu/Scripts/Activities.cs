using System;

public enum MenuActivities
{
	Empty = -1,
	Logo = 0,
	SinglePlayer = 1,
	MultiPlayer = 2,
	Notes = 3,
	Options = 4,
	Quit = 5,
	PictureLogo = 6,
	OptionsLogo = 7,
	SingleplayerLogo = 8,
	MultiplayerLogo = 9,
	NotesLogo = 10,
	ResolutionValue = 11,
	Windowed = 12,
	SoundValue = 13,
	MusicValue = 14,
	DifficultyValue = 15,
	SingleStart = 16,
	HostGame = 17,
	JoinGame = 18,
	IpValue = 19, 
	NameValue = 20,
	Back = 21,
	Save = 22
}
