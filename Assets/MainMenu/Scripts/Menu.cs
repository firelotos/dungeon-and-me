﻿using UnityEngine;
using System.Collections.Generic;

public class Menu : MonoBehaviour {
	private enum SecondMenuState {
		Opening,
		Openned,
		Closing,
		Closed
	}
	private const float MAX_LOADING_TIME = 0.5f, ALIGN = 5f;
	private SecondMenuState secondMenuState;
	private float loadingTime;
	private MenuItem[] mainMenuItems, secondMenuItems;
	private GUIStyle LogoStyle, NormalStyle, UselessStyle;
	public Font ItemFont;

	public void Start () {
		if (!Constants.isInitied) {
			print ("Error while reading constants.");
			Application.Quit();
		}
		mainMenuItems = (MenuItem[])Constants.mainMenuItems.Clone ();
		loadingTime = 0f;
		secondMenuState = SecondMenuState.Closed;
	}

	public void Update() {
		if (secondMenuState == SecondMenuState.Opening) {
			if (loadingTime < MAX_LOADING_TIME) {
				loadingTime += Time.deltaTime;
			} else {
				secondMenuState = SecondMenuState.Openned;
				loadingTime = MAX_LOADING_TIME;
			}
		} else if (secondMenuState == SecondMenuState.Closing) {
			if (loadingTime > 0f) {
				loadingTime -= Time.deltaTime;
			} else {
				secondMenuState = SecondMenuState.Closed;
				loadingTime = 0f;
				secondMenuItems = null;
			}
		}
	}
	
	public void OnGUI () {
		#region Styles init
		NormalStyle = new GUIStyle (GUI.skin.label);
		NormalStyle.font = ItemFont;
		NormalStyle.fontSize = Constants.FontSize;
		NormalStyle.normal = Constants.normalStyle;
		UselessStyle = new GUIStyle(NormalStyle);
		UselessStyle.normal = Constants.uselessStyle;
		LogoStyle = new GUIStyle (NormalStyle);
		LogoStyle.normal = Constants.logoStyle;
		#endregion

		MainMenu();
		SecondMenu();
	}

	private void SecondMenu() {
		if (secondMenuState == SecondMenuState.Closed)
			return;
		MenuItem[] items = (MenuItem[])secondMenuItems.Clone ();
		for (int i = 0; i < items.Length; i++) {
			DrawItem (i, items [i], false);
		}
	}

	private void MainMenu() {
		for (int i = 0; i < mainMenuItems.Length; i++) {
			DrawItem(i, mainMenuItems[i], true);
		}
	}

	private bool IsItemLogo(MenuItem item) {
		return item.Activity.ToString ().EndsWith ("Logo");
	}

	private void DrawItem(int i, MenuItem item, bool isMainItem) {

		float x = Screen.width / 2;
		string text = item.Text;

		if (!isMainItem && secondMenuState != SecondMenuState.Openned) {
			int newLength = (int)(text.Length / (MAX_LOADING_TIME * (loadingTime + 1) / loadingTime) * (loadingTime + 1));
			text = text.Substring(0, newLength);
		}
		GUIStyle style = new GUIStyle (NormalStyle);
		
		if (IsItemLogo(item)) {
			style = new GUIStyle(LogoStyle);
		} else if (IsUseless(item)) {
			style = new GUIStyle (UselessStyle);
		}

		Vector2 size = style.CalcSize (new GUIContent (text));

		if (!isMainItem) {
			x -= size.x + ALIGN;
		} else {
			x += ALIGN;
		}

		Rect btnRect = new Rect (x, ((float)i + 0.2f) * Screen.height / mainMenuItems.Length, size.x, size.y);

		if(!IsItemLogo(item) && btnRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y))) {
			if (isMainItem) {
				text += " |";
			} else {
				text = "| " + text;
			}
			size = style.CalcSize (new GUIContent (text));
			if (!isMainItem)
				x = Screen.width / 2 - size.x - ALIGN;
			btnRect = new Rect (x, ((float)i + 0.2f) * Screen.height / mainMenuItems.Length, size.x, size.y);
		}

		#region Value
		string value = GetValue (item);
		if (value != "" || item.Activity == MenuActivities.IpValue) {
			if (!isMainItem) {
				int newLength = (int)(value.Length / (MAX_LOADING_TIME * (loadingTime + 1) / loadingTime) * (loadingTime + 1));
				value = value.Substring(0, newLength);
			}
			GUIStyle valueStyle = new GUIStyle(style);
			valueStyle.font = ItemFont;
			valueStyle.fontSize = Constants.FontSize / 2;
			Vector2 valueSize = valueStyle.CalcSize(new GUIContent(item.Activity == MenuActivities.IpValue ? "255.255.255.255_" : value));
			float valueX = Screen.width / 2;
			if (!isMainItem) {
				valueX -= valueSize.x + ALIGN;
			} else {
				valueX += ALIGN;
			}
			if (item.Activity == MenuActivities.IpValue) {
				valueStyle.alignment = TextAnchor.MiddleRight;
				GUI.SetNextControlName("Ip value");
				string newIp = GUI.TextArea(new Rect(valueX, btnRect.y + btnRect.height, valueSize.x, valueSize.y),
				                                           value, valueStyle);
				if (loadingTime >= MAX_LOADING_TIME)
					Constants.variables.ipValue = newIp;
			} else {
				GUI.Label(new Rect(valueX, btnRect.y + btnRect.height, valueSize.x, valueSize.y), value, valueStyle);
			}
		}
		#endregion

		if (GUI.Button(btnRect, text, style)) {
			Click(item);
		}
	}

	private bool IsUseless (MenuItem item) {
		return 	item.Activity == MenuActivities.Notes;
	}

	private void Click(MenuItem item) {
		if (IsUseless (item))
			return;
		switch (item.Activity) {
		case MenuActivities.Quit:
			Application.Quit();
			break;
		case MenuActivities.Save:
			Constants.Save ();
			goto case MenuActivities.Back;
		case MenuActivities.Back:
			ChangeSecondMenu(null);
			break;
		case MenuActivities.Options:
			ChangeSecondMenu (Constants.optionsMenuItems);
			break;
		case MenuActivities.SinglePlayer:
			ChangeSecondMenu(Constants.singleplayerMenuItems);
			break;
		case MenuActivities.MultiPlayer:
			ChangeSecondMenu(Constants.multiplayerMenuItems);
			break;
		default:
			NextValue(item);
			break;
		case MenuActivities.IpValue:
			GUI.FocusControl("Ip value");
			break;
		case MenuActivities.JoinGame:
			Constants.Save();
			break;
		case MenuActivities.SingleStart:
			Constants.Save();
			Application.LoadLevel("MainGame");
			break;
		}
	}
	
	private string GetValue(MenuItem item) {
		if (IsUseless(item)) {
			return "not implemented";
		}
		switch (item.Activity) {
		case MenuActivities.Windowed:
			return Constants.variables.isWindowed.ToString();
		case MenuActivities.ResolutionValue:
			return Constants.variables.resolution.width + "x" + Constants.variables.resolution.height;
		case MenuActivities.MusicValue:
			return Constants.variables.music.ToString();
		case MenuActivities.SoundValue:
			return Constants.variables.sound.ToString();
		case MenuActivities.DifficultyValue:
			return Constants.difficulty.ToString();
		case MenuActivities.IpValue:
			return Constants.variables.ipValue;
		case MenuActivities.HostGame:
			return "Is port 12787 open?";
		}
		return "";
	}

	private void NextValue(MenuItem item) {
		switch (item.Activity) {
		case MenuActivities.ResolutionValue:
			Constants.variables.resolutionNumber = (Constants.variables.resolutionNumber + 1) % Screen.GetResolution.Length;
			break;
		case MenuActivities.Windowed:
			Constants.variables.isWindowed = !Constants.variables.isWindowed;
			break;
		case MenuActivities.MusicValue:
			Constants.variables.music = (Constants.variables.music + 10) % 110;
			break;
		case MenuActivities.SoundValue:
			Constants.variables.sound = (Constants.variables.sound + 10) % 110;
			break;
		case MenuActivities.DifficultyValue:
			Constants.difficulty = (Difficulty)((int)(Constants.difficulty + 1) % ((int)Difficulty.Hard + 1));
			break;
		}
	}

	private void ChangeSecondMenu(MenuItem[] newItems) {
		if (newItems == null || secondMenuItems != null) {
			secondMenuState = SecondMenuState.Closing;
		} else {
			if (secondMenuItems == newItems)
				return;
			secondMenuState = SecondMenuState.Opening;
			secondMenuItems = (MenuItem[])newItems.Clone();
		}
	}
}