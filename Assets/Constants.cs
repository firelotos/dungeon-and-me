using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public static class Constants
{	
	private static bool IsSaved, IsResolutionChanged;
	public static bool isInitied = Init();
	public static InputVariables inputVariables;
	public static MenuItem[] mainMenuItems, optionsMenuItems, singleplayerMenuItems, multiplayerMenuItems;
	public static GUIStyleState choisedStyle, logoStyle, uselessStyle, normalStyle;
	public static Variables variables;
	public static Difficulty difficulty;

	public static int FontSize {
		get {
			return Math.Max ((Screen.width / 32) - (Screen.width / 32) % 16, 16);
		}
	}
	public const string MainMenuFile			= "Settings/Menus/mainMenuItems.json",
						InputFile				= "Settings/input.json",
						OptionsMenuFile			= "Settings/Menus/optionsMenuItems.json",
						StylesFile				= "Settings/styles.json",
						SettingsFile			= "Settings/generalSettings.json",
						SingleplayerMenuFile	= "Settings/Menus/singleplayerMenuItems.json",
						MultiplayerMenuFile		= "Settings/Menus/multiplayerMenuItems.json",
						MultiplayerSettingsFile = "Settings/multiplayer.json";

	public class InputVariables {
		public KeyCode goUp, goLeft, goDown, goRight;
	}

	private class StylesJson {
		public string choisedColor, logoColor, uselessColor, normalColor;
	}

	public class Variables {
		private int _resolutionNumber;
		private bool _isWindowed;
		private int _sound, _music;
		private string _ipValue;

		public int resolutionNumber {
			get {
				return _resolutionNumber;
			}
			set {
				IsResolutionChanged = true;
				IsSaved = false;
				_resolutionNumber = value;
			}
		}
		public Resolution resolution {
			get {
				return Screen.GetResolution[resolutionNumber];
			}
		}
		public bool isWindowed {
			get {
				return _isWindowed;
			}
			set {
				IsResolutionChanged = true;
				IsSaved = false;
				_isWindowed = value;
			}
		}
		public int sound {
			get {
				return _sound;
			}
			set {
				IsSaved = false;
				_sound = value;
			}
		}
		public int music {
			get {
				return _music;
			}
			set {
				IsSaved = false;
				_music = value;
			}
		}
		public string ipValue {
			get {
				return _ipValue;
			}
			set {
				if (value.Length <= "255.255.255.255".Length)
					_ipValue = value;
			}
		}
	}

	public static Color ParseColor(string clr) {
		string[] rgba = clr.Split ('|');
		Color res = new Color(float.Parse (rgba [0]) / 255,
		                      float.Parse (rgba [1]) / 255,
		                      float.Parse (rgba [2]) / 255,
		                      float.Parse (rgba [3]) / 255);
		return res;
	}

	public static string ColorToString(this Color self) {
		return self.r * 255f + "|" + self.g * 255f + "|" + self.b * 255f + "|" + self.a * 255f;
	}

	#region Init
	public static bool Init() {
		if (isInitied) {
			print ("Already inited.");
		}
		if (!Directory.Exists ("Settings")) {
			Directory.CreateDirectory("Settings");
		}
		if (!Directory.Exists ("Settings/Menus")) {
			Directory.CreateDirectory("Settings/Menus");
		}
		print ("Initing constants...");
		try {
			InitGeneralSettings();
			InitInput ();
			InitMenu ();
			InitStyles ();
			Screen.SetResolution(variables.resolution.width, variables.resolution.height, !variables.isWindowed);
			difficulty = Difficulty.Medium;
			print("Sucessfull init.");
			return true;
		}
		catch (Exception ex) {
			print (ex.Message);
			return false;
		}
	}

	private static void InitGeneralSettings() {
		print ("Initing general settings.");
		
		if (File.Exists (SettingsFile)) { 
			print ("Reading " + SettingsFile);
			StreamReader varsJson = new StreamReader(SettingsFile);
			variables = JsonConvert.DeserializeObject<Variables>(varsJson.ReadToEnd());
			varsJson.Close();
		} else {
			print("General settings file doesn't exists.\nCreating default.");
			variables = new Variables();
			variables.resolutionNumber = 0;
			variables.isWindowed = true;
			variables.music = 70;
			variables.sound = 70;
			variables.ipValue = "127.0.0.1";
			string serialized = JsonConvert.SerializeObject(variables);
			StreamWriter varsJson = new StreamWriter(SettingsFile);
			varsJson.Write(serialized);
			varsJson.Close();
		}
		IsSaved = true;
		IsResolutionChanged = false;
		print ("Sucessfull general settings init.");
	}
	
	private static void InitInput() {
		print ("Initing input settings.");
		if (File.Exists (InputFile)) {
			print ("Reading " + InputFile);
			StreamReader inputJson = new StreamReader(InputFile);
			inputVariables = JsonConvert.DeserializeObject<InputVariables>(inputJson.ReadToEnd());
			inputJson.Close();
		} else {
			print("Input file doesn't exists.\nCreating default.");
			inputVariables = new InputVariables();
			inputVariables.goUp 	= KeyCode.W;
			inputVariables.goLeft	= KeyCode.A;
			inputVariables.goDown	= KeyCode.S;
			inputVariables.goRight	= KeyCode.D;
			string serialized = JsonConvert.SerializeObject(inputVariables);
			StreamWriter inputJson = new StreamWriter(InputFile);
			inputJson.Write(serialized);
			inputJson.Close();
		}
		print ("Sucessfull input settings init.");
	}

	private static void InitMenu () {
		#region Main menu
		print ("Initing main menu settings");
		if (File.Exists (MainMenuFile)) {
			print ("Reading " + MainMenuFile);
			StreamReader menuJson = new StreamReader(MainMenuFile);
			mainMenuItems = JsonConvert.DeserializeObject<MenuItem[]>(menuJson.ReadToEnd());
			menuJson.Close();
		} else {
			print("Main menu file doesn't exists.\nCreating default.");
			mainMenuItems = new MenuItem[6];
			mainMenuItems[0] = new MenuItem(MenuActivities.Logo, "Dungeon and Me");
			mainMenuItems[1] = new MenuItem(MenuActivities.SinglePlayer, "Singleplayer");
			mainMenuItems[2] = new MenuItem(MenuActivities.MultiPlayer, "Multiplayer");
			mainMenuItems[3] = new MenuItem(MenuActivities.Notes, "Notes");
			mainMenuItems[4] = new MenuItem(MenuActivities.Options, "Options");
			mainMenuItems[5] = new MenuItem(MenuActivities.Quit, "Quit");
			string serialized = JsonConvert.SerializeObject(mainMenuItems);
			StreamWriter menuJson = new StreamWriter(MainMenuFile);
			menuJson.Write(serialized);
			menuJson.Close();
		}
		#endregion
		#region Options menu
		if (File.Exists(OptionsMenuFile)) {
			print ("Reading " + OptionsMenuFile);
			StreamReader optionsJson = new StreamReader (OptionsMenuFile);
			optionsMenuItems = JsonConvert.DeserializeObject<MenuItem[]>(optionsJson.ReadToEnd());
			optionsJson.Close();
		} else {
			print("Options menu file doesn't exists.\nCreating default.");
			optionsMenuItems = new MenuItem[6];
			optionsMenuItems[0] = new MenuItem(MenuActivities.OptionsLogo, "Options");
			optionsMenuItems[1] = new MenuItem(MenuActivities.ResolutionValue, "Resolution");
			optionsMenuItems[2] = new MenuItem(MenuActivities.Windowed, "Is windowed");
			optionsMenuItems[3] = new MenuItem(MenuActivities.SoundValue, "Sound");
			optionsMenuItems[4] = new MenuItem(MenuActivities.MusicValue, "Music");
			optionsMenuItems[5] = new MenuItem(MenuActivities.Save, "Save");
			string serialized = JsonConvert.SerializeObject(optionsMenuItems);
			StreamWriter menuJson = new StreamWriter(OptionsMenuFile);
			menuJson.Write(serialized);
			menuJson.Close();
		}
		#endregion
		#region Singleplayer menu
		if (File.Exists(SingleplayerMenuFile)) {
			print ("Reading " + SingleplayerMenuFile);
			StreamReader singleJson = new StreamReader (SingleplayerMenuFile);
			singleplayerMenuItems = JsonConvert.DeserializeObject<MenuItem[]>(singleJson.ReadToEnd());
			singleJson.Close();
		} else {
			print("Singleplayer menu file doesn't exists.\nCreating default.");
			singleplayerMenuItems = new MenuItem[4];
			singleplayerMenuItems[0] = new MenuItem(MenuActivities.SingleplayerLogo, "Singleplayer");
			singleplayerMenuItems[1] = new MenuItem(MenuActivities.DifficultyValue, "Difficulty");
			singleplayerMenuItems[2] = new MenuItem(MenuActivities.SingleStart, "Start");
			singleplayerMenuItems[3] = new MenuItem(MenuActivities.Back, "Cancel");
			string serialized = JsonConvert.SerializeObject(singleplayerMenuItems);
			StreamWriter menuJson = new StreamWriter(SingleplayerMenuFile);
			menuJson.Write(serialized);
			menuJson.Close();
		}
		#endregion
		#region Multiplayer menu
		if (File.Exists(MultiplayerMenuFile)) {
			print ("Reading " + MultiplayerMenuFile);
			StreamReader multiJson = new StreamReader (MultiplayerMenuFile);
			multiplayerMenuItems = JsonConvert.DeserializeObject<MenuItem[]>(multiJson.ReadToEnd());
			multiJson.Close();
		} else {
			print("multiplayer menu file doesn't exists.\nCreating default.");
			multiplayerMenuItems 	= new MenuItem[6];
			multiplayerMenuItems[0] = new MenuItem(MenuActivities.MultiplayerLogo, "Multiplayer");
			multiplayerMenuItems[1] = new MenuItem(MenuActivities.DifficultyValue, "Difficulty");
			multiplayerMenuItems[2] = new MenuItem(MenuActivities.JoinGame, "Join game");
			multiplayerMenuItems[3] = new MenuItem(MenuActivities.IpValue, "Ip adress");
			multiplayerMenuItems[4] = new MenuItem(MenuActivities.HostGame, "Host game");
			multiplayerMenuItems[5] = new MenuItem(MenuActivities.Back, "Cancel");
			string serialized = JsonConvert.SerializeObject(multiplayerMenuItems);
			StreamWriter menuJson = new StreamWriter(MultiplayerMenuFile);
			menuJson.Write(serialized);
			menuJson.Close();
		}
		#endregion
		print ("Sucessfull menu init.");
	}
	
	private static void InitStyles () {
		StylesJson styles;
		choisedStyle = new GUIStyleState ();
		logoStyle = new GUIStyleState ();
		uselessStyle = new GUIStyleState ();
		normalStyle = new GUIStyleState ();
		
		#region File
		if (File.Exists(StylesFile)) {
			StreamReader stylesJson = new StreamReader (StylesFile);
			styles = JsonConvert.DeserializeObject<StylesJson>(stylesJson.ReadToEnd());
			stylesJson.Close();
		} else {
			print("Styles file doesn't exists.\nCreating default.");
			
			styles = new StylesJson();
			styles.choisedColor = ColorToString(Color.grey);
			styles.logoColor = ColorToString(Color.white);
			styles.normalColor = ColorToString(Color.white);
			styles.uselessColor = ColorToString(Color.grey);
			string serialized = JsonConvert.SerializeObject(styles);
			StreamWriter stylesJson = new StreamWriter(StylesFile);
			stylesJson.Write(serialized);
			stylesJson.Close();
		}
		#endregion
		
		choisedStyle.textColor = ParseColor(styles.choisedColor);
		logoStyle.textColor = ParseColor(styles.logoColor);
		uselessStyle.textColor = ParseColor(styles.uselessColor);
		normalStyle.textColor = ParseColor(styles.normalColor);
	}
	#endregion
	#region Save
	public static void Save() {
		if (IsSaved)
			return;

		SaveGeneralSettings ();
		SaveInput ();

		if (IsResolutionChanged) {
			Screen.SetResolution(variables.resolution.width,
			                     variables.resolution.height,
			                     !variables.isWindowed);
		}
		IsResolutionChanged = false;
		IsSaved = true;
	}

	private static void SaveGeneralSettings() {
		print("Saving general settings file.");
		string serialized = JsonConvert.SerializeObject(variables);
		StreamWriter varsJson = new StreamWriter(SettingsFile);
		varsJson.Write(serialized);
		varsJson.Close();
		print ("Sucessfull general settings save.");
	}

	private static void SaveInput () {
		print("Saving input file.");
		string serialized = JsonConvert.SerializeObject(inputVariables);
		StreamWriter inputJson = new StreamWriter(InputFile);
		inputJson.Write(serialized);
		inputJson.Close();
		print ("Sucessfull input settings save.");
	}
	#endregion
	
	private static void print(object o) {
		MonoBehaviour.print (o);
	}
}
