﻿using UnityEngine;

public class CharacterController2D : MonoBehaviour
{	
	#region Private
	private Camera __camera;
	public Camera mainCamera {
		get {
			if (__camera == null) {
				__camera = Camera.main;
			}
			return __camera;
		}
	}
	#endregion
	#region Public
	public Vector3 oldPosition;
	public AnimationClip Idle, Run;
	public const float MoveForce = 10;
	#endregion
	
	public void Start() {
		Constants.Init ();
		oldPosition = Vector3.zero;
	}
	
	public void Update() {
		CameraUpdate ();
		InputUpdate ();
		MouseUpdate ();
		AnimationUpdate ();
		oldPosition = transform.position;
	}

	public void CameraUpdate() {
		Vector3 newPos = transform.position;
		newPos.z = -10;
		mainCamera.transform.position = newPos;
	}
	
	public void InputUpdate() {
		if (Input.GetKey(Constants.inputVariables.goUp)) {
			rigidbody2D.AddForce(new Vector3(0, MoveForce, 0));
		}
		if (Input.GetKey (Constants.inputVariables.goDown)) {
			rigidbody2D.AddForce(new Vector3(0, -MoveForce, 0));
		}
		if (Input.GetKey (Constants.inputVariables.goLeft)) {
			rigidbody2D.AddForce(new Vector3(-MoveForce, 0, 0));
		}
		if (Input.GetKey (Constants.inputVariables.goRight)) {
			rigidbody2D.AddForce(new Vector3(MoveForce, 0, 0));
		}
		rigidbody2D.velocity /= 1.02f;
	}
	
	public void MouseUpdate() {
		//Rotating hero to cursor
		//Look at notes
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = -10;
		mousePos = mainCamera.ScreenToWorldPoint (mousePos);
		float	a = mousePos.x - transform.position.x,
				b = mousePos.y - transform.position.y;

		float atan = (Input.mousePosition.x > Screen.width / 2 ? Mathf.PI : 0) + Mathf.Atan (b / a);

		float angle = atan * 180 / Mathf.PI;
		transform.eulerAngles = new Vector3 (0, 0, angle);
	}
	
	public void AnimationUpdate() {
		Vector3 posDelta = oldPosition - transform.position;
		float delta = Mathf.Sqrt (posDelta.x * posDelta.x + posDelta.y * posDelta.y);
		if (delta > 0) {
			animation.Play("Run");
			animation ["Run"].speed = delta * 10;
		} else {
			animation.Play("Idle");
		}
	}
}